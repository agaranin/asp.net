﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeInventoryExam.Models;
using HomeInventoryExam.Helpers;

namespace HomeInventoryExam.Controllers
{
    public class HomeItemsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: HomeItems
        public ActionResult Index(string sortOrder, string sortDir, string searchString)
        {
            ViewBag.SearchString = searchString;
            ViewBag.sortOrder = sortOrder;
            ViewBag.sortDir = sortDir;

            var homeItems = db.HomeItems.Include(h => h.Location).Include(h => h.PurchaseInfo).AsQueryable();
            if (!string.IsNullOrEmpty(searchString))
                homeItems = homeItems.Where(h => h.Description.Contains(searchString));

            if (sortOrder != null)
            {
                switch (sortOrder.ToLower())
                {
                    case "description":
                        if (sortDir.ToLower() == "desc")
                            homeItems = homeItems.OrderByDescending(h => h.Description);
                        else
                            homeItems = homeItems.OrderBy(h => h.Description);
                        break;
                    case "location":
                        if (sortDir.ToLower() == "desc")
                            homeItems = homeItems.OrderByDescending(h => h.Location.Name);
                        else
                            homeItems = homeItems.OrderBy(h => h.Location.Name);
                        break;
                    case "model":
                        if (sortDir.ToLower() == "desc")
                            homeItems = homeItems.OrderByDescending(h => h.Model);
                        else
                            homeItems = homeItems.OrderBy(h => h.Model);
                        break;
                    case "serialnumber":
                        if (sortDir.ToLower() == "desc")
                            homeItems = homeItems.OrderByDescending(h => h.SerialNumber);
                        else
                            homeItems = homeItems.OrderBy(h => h.SerialNumber);
                        break;
                }
            }
            return View(homeItems.ToList());
        }

        // GET: HomeItems/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HomeItem homeItem = db.HomeItems.Find(id);
            if (homeItem == null)
            {
                return HttpNotFound();
            }
            return View(homeItem);
        }

        // GET: HomeItems/Create
        public ActionResult Create()
        {
            ViewBag.LocationId = new SelectList(db.Locations, "LocationId", "Name");
            ViewBag.PurchaseInfoId = new SelectList(db.PurchaseInfos, "PurchaseInfoId", "Where");
            return View();
        }

        // POST: HomeItems/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HomeItemViewModel homeItemVM)
        {
            if (ModelState.IsValid)
            {
                HomeItem homeItem = new HomeItem();
                homeItem.LocationId = homeItemVM.LocationId;
                homeItem.Description = homeItemVM.Description;
                homeItem.Model = homeItemVM.Model;
                homeItem.SerialNumber = homeItemVM.SerialNumber;
                if (homeItem.Photo != null)
                    homeItem.Photo = ImageConverter.ByteArrayFromPostedFile(homeItemVM.Photo);
                PurchaseInfo purchaseInfo = new PurchaseInfo();
                purchaseInfo.When = (DateTime)homeItemVM.When;
                purchaseInfo.Where = homeItemVM.Where;
                purchaseInfo.Warranty = homeItemVM.Warranty;
                purchaseInfo.Price = homeItemVM.Price;
                homeItem.PurchaseInfo = purchaseInfo;
                db.HomeItems.Add(homeItem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LocationId = new SelectList(db.Locations, "LocationId", "Name", homeItemVM.LocationId);
            return View(homeItemVM);
        }

        // GET: HomeItems/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HomeItem homeItem = db.HomeItems.Find(id);
            if (homeItem == null)
            {
                return HttpNotFound();
            }
            HomeItemViewModel homeItemViewModel = new HomeItemViewModel();
            homeItemViewModel.LocationId = homeItem.LocationId;
            homeItemViewModel.Description = homeItem.Description;
            homeItemViewModel.Model = homeItem.Model;
            homeItemViewModel.SerialNumber = homeItem.SerialNumber;
            ViewBag.LocationId = new SelectList(db.Locations, "LocationId", "Name", homeItem.LocationId);
            return View(homeItem);
        }

        // POST: HomeItems/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(HomeItemViewModel homeItemVM)
        {
            if (ModelState.IsValid)
            {
                HomeItem homeItem = db.HomeItems.Find(homeItemVM.HomeItemId);
                homeItem.LocationId = homeItemVM.LocationId;
                homeItem.Description = homeItemVM.Description;
                homeItem.Model = homeItemVM.Model;
                homeItem.SerialNumber = homeItemVM.SerialNumber;
                if (homeItem.Photo != null)
                    homeItem.Photo = ImageConverter.ByteArrayFromPostedFile(homeItemVM.Photo);
                PurchaseInfo purchaseInfo = new PurchaseInfo();
                purchaseInfo.When = (DateTime)homeItemVM.When;
                purchaseInfo.Where = homeItemVM.Where;
                purchaseInfo.Warranty = homeItemVM.Warranty;
                purchaseInfo.Price = homeItemVM.Price;
                homeItem.PurchaseInfo = purchaseInfo;
                db.Entry(homeItem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LocationId = new SelectList(db.Locations, "LocationId", "Name", homeItemVM.LocationId);
            return View(homeItemVM);
        }

        // GET: HomeItems/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HomeItem homeItem = db.HomeItems.Find(id);
            if (homeItem == null)
            {
                return HttpNotFound();
            }
            return View(homeItem);
        }

        // POST: HomeItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            HomeItem homeItem = db.HomeItems.Find(id);
            db.HomeItems.Remove(homeItem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
