namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePostModel1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Posts", "UserFullName", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Posts", "UserFullName", c => c.String(nullable: false));
        }
    }
}
