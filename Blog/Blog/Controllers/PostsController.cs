﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Blog.Helpers;
using Blog.Models;

namespace Blog.Controllers
{
    public class PostsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Posts
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult Index()
        {
            return View(db.Posts.ToList());
        }
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult OnlyPublic()
        {
            var posts = db.Posts.Where(p => p.PostedOn.HasValue).OrderByDescending(p => p.PostedOn).ToList();
            return View("OnlyPublic", posts);
        }
         
        public ActionResult Full(int id)
        {
            var post = db.Posts.Include(p => p.Comments).Where(p => p.Id == id).First();
            return View("FullPost", post);
        }

        // GET: Posts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Include(p => p.Comments).Where(p => p.Id == id).First();
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }
        [Authorize(Roles = RoleName.CanManage)]
        // GET: Posts/Create
        public ActionResult Create()
        {
            //var p = new Post();
            //p.CreatedOn = DateTime.Now;
            //p.UpdatedOn = DateTime.Now;
            //p.PostedOn = null;
            //p.UserFullName = UserHelper.GetUserName(db.Users, User.Identity);
            //return View(p);
            return View();
        }

        // POST: Posts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = RoleName.CanManage)]
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Title,Content,PostedOn")] CreatePostViewModel createPostViewModel)
        {
            if (ModelState.IsValid)
            {
                Post post = new Post();
                post.CreatedOn = DateTime.Now;
                post.UpdatedOn = DateTime.Now;
                post.Title = createPostViewModel.Title;
                post.Content = createPostViewModel.Content;
                post.PostedOn = createPostViewModel.PostedOn;
                post.UserFullName = UserHelper.GetUserName(db.Users, User.Identity);
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(createPostViewModel);
        }

        [Authorize(Roles = RoleName.CanManage)]
        // GET: Posts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateInput(false)]
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult Edit([Bind(Include = "Id,Title,Content,CreatedOn,UpdatedOn,PostedOn,UserFullName")] Post post)
        {
            if (ModelState.IsValid)
            {
                post.UpdatedOn = DateTime.Now;
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(post);
        }

        // GET: Posts/Delete/5
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = RoleName.CanManage)]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
