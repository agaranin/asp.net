﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Comment
    {
        public int Id { get; set; }
        [Required]
        public string Content { get; set; }
        [Display(Name = "Created on")]
        public DateTime CreatedOn { get; set; }
        [Display(Name = "Updated on")]
        public DateTime UpdatedOn { get; set; }
        [Display(Name = "Full Name")]
        [StringLength(255)]
        public string UserFullName { get; set; }
        public Post Post { get; set; }

        [Display(Name = "Post")]
        [Required]
        public int PostId { get; set; }
        public bool IsPublished { get; set; }
    }
}