﻿using MVCSample1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCSample1.Controllers
{
    public class CalculatorController : Controller
    {
        // GET: Calculator
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Calculator calculator)
        {
            double result = 0;
            double.TryParse(calculator.Operator1, out double x);
            double.TryParse(calculator.Operator2, out double y);
            switch (calculator.Operand) 
            {
                case "+":
                    result = x + y;
                    break;
                case "-":
                    result = x - y;
                    break;
                case "*":
                    result = x * y;
                    break;
                case "/":
                    result = x / y;
                    break;
            }
            ViewBag.Result = result;
            return View("Result", result);
        }
    }
}