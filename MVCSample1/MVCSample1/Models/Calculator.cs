﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCSample1.Models
{
    public class Calculator
    {
        [DisplayName("Operator 1")]
        [Required]
        //[Range(1,100)]
        public string  Operator1 { get; set; }
        [Display(Name ="Operator 2")]
        [Required]
        //[Range(1, 100)]
        public string Operator2 { get; set; }
        [Required]
        public string Operand { get; set; }
    }

}