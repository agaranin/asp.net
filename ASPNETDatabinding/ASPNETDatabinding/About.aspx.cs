﻿using ASPNETDatabinding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ASPNETDatabinding
{
    public partial class About : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }

        private List<Product> GetProducts(string criteria)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            if (string.IsNullOrEmpty(criteria))
            {
                return db.Products.ToList();
            }
            else
            {
                return db.Products.Where(p => p.Name.StartsWith(criteria)).ToList();

            }
        }
        private void BindGrid()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            grvProducts.DataSource = db.Products.ToList();
            grvProducts.DataBind();
        }

        protected void btnSubmit_Click1(object sender, EventArgs e)
        {
            BindGrid();
        }
    }
}