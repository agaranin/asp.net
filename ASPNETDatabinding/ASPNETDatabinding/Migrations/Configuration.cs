namespace ASPNETDatabinding.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ASPNETDatabinding.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ASPNETDatabinding.Models.ApplicationDbContext context)
        {
            context.Products.AddOrUpdate(
                new Models.Product() { Name = "Paper", Description = ""},
                new Models.Product() { Name = "Notebook", Description = "" },
                new Models.Product() { Name = "Pencil", Description = "Package of 12 pencils" }
                );
            context.SaveChanges();
        }
    }
}
