﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ASPNETDatabinding.Startup))]
namespace ASPNETDatabinding
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
