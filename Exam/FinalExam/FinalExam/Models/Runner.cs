﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalExam.Models
{
    public class Runner
    {
        public int RunnerId { get; set; }
        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(100)]
        public string LastName { get; set; }
        [Required]
        public DateTime BirthDate { get; set; }
        [Required]
        public Gender Gender { get; set; }
        [Required]
        [StringLength(100)]
        public string Email { get; set; }
        [Required]
        public string Telephone { get; set; }
        [Required]
        [StringLength(255)]
        public string City { get; set; }
        [Required]
        [StringLength(255)]
        public string Address { get; set; }
        public string PostalCode { get; set; }
        [Required]
        public int CountryId { get; set; }
        [Required]
        public DateTime RegistrationDate { get; set; }
        [Required]
        public string ContactPersonName { get; set; }
        [Required]
        public string ContactPersonTelephone { get; set; }
        public int EventId { get; set; }
        public Event Event { get; set; }
        public Country Country { get; set; }
    }
}