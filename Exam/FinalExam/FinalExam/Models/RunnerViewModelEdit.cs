﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalExam.Models
{
    public class RunnerViewModelEdit
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public int CountryId { get; set; }
        public string City { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string ContactPersonName { get; set; }
        public string ContactPersonTelephone { get; set; }
        public int EventId { get; set; }
    }
}