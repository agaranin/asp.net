﻿namespace FinalExam.Migrations
{
    using FinalExam.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<FinalExam.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(FinalExam.Models.ApplicationDbContext context)
        {
            context.Countries.AddOrUpdate(c => c.Name,
                new Country()
                {
                    Name = "Canada"
                },
                new Country()
                {
                    Name = "USA"
                },
                new Country()
                {
                    Name = "Russia"
                },
                new Country()
                {
                    Name = "Peru"
                }
                );
            context.SaveChanges();
            base.Seed(context);
        }
    }
}
