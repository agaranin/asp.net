﻿namespace FinalExam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                        EventDate = c.DateTime(nullable: false),
                        IsClosed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Runners",
                c => new
                    {
                        RunnerId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        BirthDate = c.DateTime(nullable: false),
                        Gender = c.Int(nullable: false),
                        Email = c.String(nullable: false, maxLength: 100),
                        Telephone = c.String(nullable: false),
                        City = c.String(nullable: false, maxLength: 255),
                        Address = c.String(nullable: false, maxLength: 255),
                        PostalCode = c.String(),
                        CountryId = c.Int(nullable: false),
                        RegistrationDate = c.DateTime(nullable: false),
                        ContactPersonName = c.String(nullable: false),
                        ContactPersonTelephone = c.String(nullable: false),
                        EventId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RunnerId)
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: true)
                .ForeignKey("dbo.Events", t => t.EventId, cascadeDelete: true)
                .Index(t => t.CountryId)
                .Index(t => t.EventId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Runners", "EventId", "dbo.Events");
            DropForeignKey("dbo.Runners", "CountryId", "dbo.Countries");
            DropIndex("dbo.Runners", new[] { "EventId" });
            DropIndex("dbo.Runners", new[] { "CountryId" });
            DropTable("dbo.Runners");
            DropTable("dbo.Events");
            DropTable("dbo.Countries");
        }
    }
}
