﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AdmissionSystem.Models
{
    public class Department
    {
        public int DepartmentId { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        public ICollection<Program> Programs { get; set; }
    }
}