﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace AdmissionSystem.Models
{
    public class Application
    {
        [ForeignKey("User")]
        [StringLength(128)]
        public string ApplicationId { get; set; }
        [StringLength(255)]
        public string FirstName { get; set; }
        [StringLength(255)]
        public string LastName { get; set; }
        [StringLength(20)]
        public string TelePhoneNumber { get; set; }
        public byte[] Photo { get; set; }
        public int DepartmentId { get; set; }
        public Department Department { get; set; }
        public int ProgramId { get; set; }
        public Program Program { get; set; }
        public DateTime RegistrationDate { get; set; }

        [Column("Status")]
        [StringLength(50)]
        public string StatusString
        {
            get { return Status.ToString(); }
            private set { Status = (Status)Enum.Parse(typeof(Status), value); }
        }

        [NotMapped]
        public Status Status { get; set; }

        public ICollection<EducationDetail> EducationDetails { get; set; }
        public ICollection<EnclosedDocument> EnclosedDocuments { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}