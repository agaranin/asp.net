﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdmissionSystem.Models
{
    public class EducationDetail
    {
        public int Id { get; set; }
        [StringLength(128)]
        public string ApplicationId { get; set; }
        public Application Application { get; set; }
        [StringLength(255)]
        public string Qualification { get; set; }
        public int Year { get; set; }
        public double Duration { get; set; }
        [StringLength(255)]
        [DisplayName("Board University")]
        public string BoardUniversity { get; set; }
        [StringLength(500)]
        public string Subjects { get; set; }
        public int Percentage { get; set; }
    }
}