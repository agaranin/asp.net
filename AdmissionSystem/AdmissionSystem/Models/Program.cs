﻿using System.ComponentModel.DataAnnotations;

namespace AdmissionSystem.Models
{
    public class Program
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        public int DepartmentId { get; set; }
        public Department Department { get; set; }
    }
}