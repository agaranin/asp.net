﻿namespace AdmissionSystem.Models
{
    public enum Status
    {
        Draft,
        Saved,
        Received,
        Approved
    }
}