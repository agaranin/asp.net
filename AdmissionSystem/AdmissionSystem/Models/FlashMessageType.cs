﻿namespace AdmissionSystem.Models
{
    public enum FlashMessageType
    {
        Success,
        Warning,
        Info,
        Danger
    }
}