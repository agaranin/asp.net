﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AdmissionSystem.Models
{
    public class EnclosedDocument
    {
        public int Id { get; set; }
        [StringLength(36)]
        public string ApplicationId { get; set; }
        public Application Application { get; set; }
        [StringLength(50)]
        [DisplayName("Document Type")]
        public string DocumentType { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [DisplayName("Document File")]
        public byte[] DocumentFile { get; set; }
    }
}