﻿using AdmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace AdmissionSystem.ViewModels
{
    public class EnclosedDocumentsViewModel
    {
        public int Id { get; set; }
        public string ApplicationId { get; set; }
        [DisplayName("Document Type")]
        public string DocumentType { get; set; }
        public string Name { get; set; }
        [DisplayName("Document File")]
        public HttpPostedFileBase DocumentFile { get; set; }
    }
}