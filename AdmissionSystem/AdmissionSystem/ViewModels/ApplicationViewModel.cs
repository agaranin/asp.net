﻿using AdmissionSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;

namespace AdmissionSystem.ViewModels
{
    public class ApplicationViewModel
    {
        [DisplayName("Application Id")]
        public string ApplicationId { get; set; }
        public int DepartmentId { get; set; }
        [DisplayName("Department Name")]
        public string DepartmentName { get; set; }
        public int ProgramId { get; set; }
        [DisplayName("Program Name")]
        public string ProgramName { get; set; }
        public string Email { get; set; }
        [DisplayName("First Name")]
        public string FirstName { get; set; }
        [DisplayName("Last Name")]
        public string LastName { get; set; }
        [DisplayName("Student Name")]
        public string StudentName { get; set; }
        [DisplayName("Phone Number")]
        public string TelePhoneNumber { get; set; }
        public HttpPostedFileBase Photo { get; set; }
        [DisplayName("Registration Date")]
        public DateTime RegistrationDate { get; set; }
        public byte[] PhotoDb { get; set; }
        public Status Status { get; set; }
        public ICollection<EducationDetail> EducationDetails { get; set; }
        public ICollection<EnclosedDocument> EnclosedDocuments { get; set; }
    }
}