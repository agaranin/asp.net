namespace AdmissionSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeDateField : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Applications", "RegistrationDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Applications", "RegistrationDate", c => c.DateTime());
        }
    }
}
