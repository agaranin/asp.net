namespace AdmissionSystem.Migrations
{
    using AdmissionSystem.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AdmissionSystem.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AdmissionSystem.Models.ApplicationDbContext context)
        {
            if (!context.Roles.Any(r => r.Name == RoleName.CanManage))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = RoleName.CanManage };

                manager.Create(role);
            }

            if (!context.Users.Any(u => u.UserName == "admin@admin.com"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = "admin@admin.com" };

                manager.Create(user, "SecretPass1!");
                manager.AddToRole(user.Id, RoleName.CanManage);
            }
            context.Departments.AddOrUpdate(d => d.Name,
            new Department()
            {
                DepartmentId = 1,
                Name = "IT"
            },
            new Department()
            {
                DepartmentId = 2,
                Name = "Menagement"
            },
            new Department()
            {
                DepartmentId = 3,
                Name = "Finance"
            }
            );
            context.SaveChanges();
            context.Programs.AddOrUpdate(d => d.Name,
              new Program()
              {
                  Id = 1,
                  Name = "FullStack Developer",
                  DepartmentId = 1
              },
              new Program()
              {
                  Id = 2,
                  Name = "Mobile Developer",
                  DepartmentId = 1
              },
              new Program()
              {
                  Id = 3,
                  Name = "Web Designer",
                  DepartmentId = 1
              },
              new Program()
              {
                  Id = 4,
                  Name = "Entrepreneurship",
                  DepartmentId = 2
              },
              new Program()
              {
                  Id = 5,
                  Name = "Human Resource",
                  DepartmentId = 2
              },
              new Program()
              {
                  Id = 6,
                  Name = "Business law",
                  DepartmentId = 2
              },
              new Program()
              {
                  Id = 7,
                  Name = "Investment",
                  DepartmentId = 3
              },
              new Program()
              {
                  Id = 8,
                  Name = "International Finance",
                  DepartmentId = 3
              },
              new Program()
              {
                  Id = 9,
                  Name = "Personal Finance",
                  DepartmentId = 3
              }

              );


            base.Seed(context);
            context.SaveChanges();
        }
    }
}
