﻿using AdmissionSystem.Models;
using System.Web.Mvc;

namespace AdmissionSystem.Controllers
{
    public class ControllerBase : Controller
    {
        // GET: ControllerBase
        public void SetFlash(FlashMessageType type, string text)
        {
            TempData["FlashMessage.Type"] = type;
            TempData["FlashMessage.Text"] = text;
        }
    }
}