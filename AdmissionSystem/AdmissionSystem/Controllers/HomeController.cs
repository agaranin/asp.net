﻿using AdmissionSystem.Models;
using Microsoft.AspNet.Identity;
using System.Web.Mvc;

namespace AdmissionSystem.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            if (User.IsInRole(RoleName.CanManage))
            {
                return RedirectToAction("Index", "Applications");
            }
            else
            {
                return RedirectToAction("Edit", "Applications", User.Identity.GetUserId());
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}