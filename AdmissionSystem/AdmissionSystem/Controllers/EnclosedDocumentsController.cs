﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdmissionSystem.Models;

namespace AdmissionSystem.Controllers
{
    public class EnclosedDocumentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: EnclosedDocuments
        public ActionResult Index()
        {
            var enclosedDocuments = db.EnclosedDocuments.Include(e => e.Application);
            return View(enclosedDocuments.ToList());
        }

        // GET: EnclosedDocuments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EnclosedDocument enclosedDocument = db.EnclosedDocuments.Find(id);
            if (enclosedDocument == null)
            {
                return HttpNotFound();
            }
            return View(enclosedDocument);
        }

        // GET: EnclosedDocuments/Create
        public ActionResult Create()
        {
            ViewBag.ApplicationId = new SelectList(db.Applications, "ApplicationId", "FirstName");
            return View();
        }

        // POST: EnclosedDocuments/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ApplicationId,DocumentType,Name,DocumentFile")] EnclosedDocument enclosedDocument)
        {
            if (ModelState.IsValid)
            {
                db.EnclosedDocuments.Add(enclosedDocument);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ApplicationId = new SelectList(db.Applications, "ApplicationId", "FirstName", enclosedDocument.ApplicationId);
            return View(enclosedDocument);
        }

        // GET: EnclosedDocuments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EnclosedDocument enclosedDocument = db.EnclosedDocuments.Find(id);
            if (enclosedDocument == null)
            {
                return HttpNotFound();
            }
            ViewBag.ApplicationId = new SelectList(db.Applications, "ApplicationId", "FirstName", enclosedDocument.ApplicationId);
            return View(enclosedDocument);
        }

        // POST: EnclosedDocuments/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ApplicationId,DocumentType,Name,DocumentFile")] EnclosedDocument enclosedDocument)
        {
            if (ModelState.IsValid)
            {
                db.Entry(enclosedDocument).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ApplicationId = new SelectList(db.Applications, "ApplicationId", "FirstName", enclosedDocument.ApplicationId);
            return View(enclosedDocument);
        }

        // GET: EnclosedDocuments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EnclosedDocument enclosedDocument = db.EnclosedDocuments.Find(id);
            if (enclosedDocument == null)
            {
                return HttpNotFound();
            }
            return View(enclosedDocument);
        }

        // POST: EnclosedDocuments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EnclosedDocument enclosedDocument = db.EnclosedDocuments.Find(id);
            db.EnclosedDocuments.Remove(enclosedDocument);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
