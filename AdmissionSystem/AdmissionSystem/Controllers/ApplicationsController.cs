﻿using AdmissionSystem.Helpers;
using AdmissionSystem.Models;
using AdmissionSystem.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace AdmissionSystem.Controllers
{
    public class ApplicationsController : ControllerBase
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Applications
        public ActionResult Index(string StatusFilter, string ApplicationIdFilter, string StudentNameFilter)
        {
            var applications = db.Applications.Include(a => a.Department).Include(a => a.Program).Include(a => a.User);
            List<ApplicationViewModel> applicationVM = new List<ApplicationViewModel>();
            foreach (var item in applications)
            {
                applicationVM.Add(RetriveApplicationVM(item.ApplicationId));
            }
            var query = applicationVM.AsQueryable();
            if (!string.IsNullOrEmpty(StudentNameFilter))
                query = query.Where(a => a.StudentName.Contains(StudentNameFilter));
            if (!string.IsNullOrEmpty(ApplicationIdFilter))
                query = query.Where(a => a.ApplicationId.Contains(ApplicationIdFilter));
            if (!string.IsNullOrEmpty(StatusFilter))
                if(!StatusFilter.Equals("--All Statuses--"))
                    query = query.Where(a => Enum.GetName(typeof(Status), a.Status).Contains(StatusFilter));

            List<string> statusList = new List<string>();
            statusList.Add("--All Statuses--");
            foreach (var item in Enum.GetValues(typeof(Status)))
            {
            statusList.Add(Enum.GetName(typeof(Status), item));
            }
            ViewBag.StatusFilter = new SelectList(statusList, StatusFilter);
            ViewBag.ApplicationIdFilter = ApplicationIdFilter;
            ViewBag.StudentNameFilter = StudentNameFilter;

            if (Request.IsAjaxRequest())
                return PartialView("_ApplicationsList", query);
            else
                return View(query);
        }

        [Authorize]
        private ApplicationViewModel RetriveApplicationVM(string id)
        {
            if (id == null)
                id = User.Identity.GetUserId();
            Application application = db.Applications
                .Include(a => a.EducationDetails)
                .Include(a => a.EnclosedDocuments)
                .Include(a => a.Department)
                .Include(a => a.Program)
                .Where(a => a.ApplicationId == id)
                .SingleOrDefault();
            string email = db.Users.Where(u => u.Id == id).Select(u => u.Email).FirstOrDefault();
            ApplicationViewModel applicationViewModel = new ApplicationViewModel()
            {
                ApplicationId = application.ApplicationId,
                DepartmentId = application.Department != null ? application.DepartmentId : 1,
                DepartmentName = application.Department.Name,
                ProgramId = application.Program != null ? application.ProgramId : 1,
                ProgramName = application.Program.Name,
                Email = email,
                FirstName = application.FirstName,
                LastName = application.LastName,
                StudentName = application.FirstName + " " + application.LastName,
                TelePhoneNumber = application.TelePhoneNumber,
                Status = application.Status,
                PhotoDb = application.Photo,
                EducationDetails = application.EducationDetails,
                EnclosedDocuments = application.EnclosedDocuments,
                RegistrationDate = application.RegistrationDate
            };
            applicationViewModel.PhotoDb = application.Photo;
            //populateDropDownList(applicationViewModel.DepartmentId);
            return applicationViewModel;
        }

        private void populateDropDownList(int? departmentId)
        {
            throw new NotImplementedException();
        }

        // GET: Applications/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // GET: Applications/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Name");
            ViewBag.ProgramId = new SelectList(db.Programs, "Id", "Name");
            ViewBag.ApplicationId = new SelectList(db.Users, "Id", "Email");
            return View();
        }

        // POST: Applications/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ApplicationId,FirstName,LastName,TelePhoneNumber,Photo,DepartmentId,ProgramId,RegistrationDate,StatusString")] Application application)
        {
            if (ModelState.IsValid)
            {
                db.Applications.Add(application);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Name", application.DepartmentId);
            ViewBag.ProgramId = new SelectList(db.Programs, "Id", "Name", application.ProgramId);
            ViewBag.ApplicationId = new SelectList(db.Users, "Id", "Email", application.ApplicationId);
            return View(application);
        }
        [Authorize]
        // GET: Applications/Edit/5
        public ActionResult Edit(string id, string tab)
        {
            if (id == null)
            {
                id = User.Identity.GetUserId();
            }
            Application application = db.Applications.Find(id);
            ApplicationViewModel applicationVM = RetriveApplicationVM(id);

            if (application == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Name", applicationVM.DepartmentId);
            ViewBag.ProgId = applicationVM.ProgramId;
            ViewBag.ProgramId = new SelectList(db.Programs, "Id", "Name", applicationVM.ProgramId);
            if (!string.IsNullOrEmpty(tab))
            {
                if (tab.Equals("EducationDetails"))
                    return View("EditEducationActive", applicationVM);
                if (tab.Equals("EnclosedDocuments"))
                    return View("EditDucumentsActive", applicationVM);
            }
            return View(applicationVM);
        }
    // POST: Applications/Edit/5
    // To protect from overposting attacks, enable the specific properties you want to bind to, for 
    // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit(ApplicationViewModel applicationVM)
        {
            if (ModelState.IsValid)
            {
                Application application = db.Applications.Include(a => a.EducationDetails).Include(a => a.EnclosedDocuments).Where(a => a.ApplicationId == applicationVM.ApplicationId).SingleOrDefault();
                application.FirstName = applicationVM.FirstName;
                application.LastName = applicationVM.LastName;
                application.TelePhoneNumber = applicationVM.TelePhoneNumber;
                application.DepartmentId = applicationVM.DepartmentId;
                application.ProgramId = applicationVM.ProgramId;
                application.Status = applicationVM.Status;
                if (applicationVM.Photo != null)
                    application.Photo = ImageConverter.ByteArrayFromPostedFile(applicationVM.Photo);
                db.Entry(application).State = EntityState.Modified;
                db.SaveChanges();
                SetFlash(FlashMessageType.Success, "The personal information has been saved successfully!");
                return RedirectToAction("Edit", "Applications", User.Identity.GetUserId());
            }
            ViewBag.DepartmentId = new SelectList(db.Departments, "DepartmentId", "Name", applicationVM.DepartmentId);
            ViewBag.ProgramId = new SelectList(db.Programs, "Id", "Name", applicationVM.ProgramId);
            return View(applicationVM);
        }

        [Authorize]
        // GET
        public ActionResult CreateDetails()
        {
           
            return View("CreateEducationDetails");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult CreateDetails(EducationDetail educationDetail)
        {
            if (ModelState.IsValid)
            {
                var userId = User.Identity.GetUserId();
                var application = db.Applications.Include(a => a.EducationDetails).Where(a => a.ApplicationId == userId).FirstOrDefault();
                application.EducationDetails.Add(educationDetail);
                db.SaveChanges();
                SetFlash(FlashMessageType.Success, "Educational detail has been successfully added!");
                return RedirectToAction("Edit", new { tab = "EducationDetails" });
            }
            return View("CreateEducationDetails", educationDetail);
        }

        // GET: EducationDetails/Edit/5
        [Authorize]
        public ActionResult EditEducationDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EducationDetail educationDetail = db.EducationDetails.Find(id);
            if (educationDetail == null)
            {
                return HttpNotFound();
            }
            return View(educationDetail);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult EditEducationDetails(EducationDetail educationDetail)
        {
            if (ModelState.IsValid)
            {
                db.Entry(educationDetail).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Edit", new { tab = "EducationDetails" });
            }
            return View(educationDetail);
        }
        // GET: EducationDetails/Delete/5
        public ActionResult DeleteEducationDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EducationDetail educationDetail = db.EducationDetails.Find(id);
            if (educationDetail == null)
            {
                return HttpNotFound();
            }
            return View(educationDetail);
        }

        // POST: EducationDetails/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteEducationDetails(int id)
        {
            EducationDetail educationDetail = db.EducationDetails.Find(id);
            db.EducationDetails.Remove(educationDetail);
            db.SaveChanges();
            SetFlash(FlashMessageType.Danger, "Educational detail has been successfully deleted!");
            return RedirectToAction("Edit", new { tab = "EducationDetails" });
        }

        [Authorize]
        public ActionResult CreateEnclosedDocuments()
        {
            return View();
        }

        // POST: EnclosedDocuments/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult CreateEnclosedDocuments(EnclosedDocumentsViewModel enclosedDocumentsViewModel)
        {
            if (ModelState.IsValid)
            {
                var enclosedDocument = new EnclosedDocument();
                enclosedDocument.DocumentType = enclosedDocumentsViewModel.DocumentType;
                if (enclosedDocumentsViewModel.DocumentFile != null)
                {
                    enclosedDocument.DocumentFile = ImageConverter.ByteArrayFromPostedFile(enclosedDocumentsViewModel.DocumentFile);
                    enclosedDocument.Name = enclosedDocumentsViewModel.DocumentFile.FileName;
                }
                var userId = User.Identity.GetUserId();
                var application = db.Applications.Include(a => a.EnclosedDocuments).Where(a => a.ApplicationId == userId).FirstOrDefault();
                application.EnclosedDocuments.Add(enclosedDocument);

                db.SaveChanges();
                SetFlash(FlashMessageType.Success, "Document has been successfully added!");
                return RedirectToAction("Edit", new { tab = "EnclosedDocuments" });
            }

            return View(enclosedDocumentsViewModel);
        }

        [HttpGet]
        public FileResult DownLoadFile(int id)
        {
            var FileById = db.EnclosedDocuments.Find(id);

            return File(FileById.DocumentFile, "application/octet-stream", FileById.Name);

        }

        public ActionResult SaveApplication(string id)
        {
            Application application = db.Applications.Find(id);
            application.Status = Status.Saved;
            db.SaveChanges();
            SetFlash(FlashMessageType.Success, "Application has been successfully saved!");
            return RedirectToAction("Edit", new { tab = "EnclosedDocuments" });
        }

        public ActionResult FinishApplication(string id)
        {
            Application application = db.Applications.Find(id);
            application.Status = Status.Received;
            db.SaveChanges();
            SetFlash(FlashMessageType.Success, "Application has been successfully received!");
            return RedirectToAction("Edit", new { tab = "EnclosedDocuments" });
        }

        [HttpGet]
        public JsonResult GetDepartments()
        {
            var departments = db.Departments.ToList();
            departments.Insert(0, new Department() { DepartmentId = 0, Name = "--All Departments--" });
            var result = (from r in departments
                          select new
                          {
                              id = r.DepartmentId,
                              name = r.Name
                          }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProgramsByDepartmentId(int id)
        {
            var programs = GetProgramsByDepId(id);
            var result = (from r in programs
                          select new
                          {
                              id = r.Id,
                              name = r.Name
                          }).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public List<Program> GetProgramsByDepId(int id)
        {
            List<Program> programs = new List<Program>();
            if (id > 0)
                programs = db.Programs.Where(p => p.DepartmentId == id).ToList();
            else
                programs.Insert(0, new Program { Id = 0, Name = "--Select a program--" });

            return programs;
        }

        // GET: Applications/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = db.Applications.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // POST: Applications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Application application = db.Applications.Find(id);
            db.Applications.Remove(application);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
