﻿namespace HomeInventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Inventories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Photo = c.Binary(),
                        Description = c.String(nullable: false, maxLength: 255),
                        Location = c.String(nullable: false, maxLength: 255),
                        Model = c.String(nullable: false, maxLength: 255),
                        SerialNumber = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PurchaseInfos",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        When = c.DateTime(nullable: false),
                        Where = c.String(nullable: false, maxLength: 255),
                        Warranty = c.String(nullable: false, maxLength: 255),
                        Price = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Inventories", t => t.Id)
                .Index(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PurchaseInfos", "Id", "dbo.Inventories");
            DropIndex("dbo.PurchaseInfos", new[] { "Id" });
            DropTable("dbo.PurchaseInfos");
            DropTable("dbo.Inventories");
        }
    }
}
