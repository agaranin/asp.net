﻿namespace HomeInventory.Migrations
{
    using HomeInventory.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<HomeInventory.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(HomeInventory.Models.ApplicationDbContext context)
        {
            context.Inventories.AddOrUpdate(
                new Inventory() { Id = 1, Description = "Wood table", Location = "Salon", Model = "woodOak", SerialNumber = 123456 }
                );
            context.PurchaseInfos.AddOrUpdate(
                new PurchaseInfo() { Id = 1, When = DateTime.Now, Where = "Home", Warranty = "5 years", Price = 1000 }
                );
            context.SaveChanges();
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
        }
    }
}
