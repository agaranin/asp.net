﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace HomeInventory.Models
{
    [Table("PurchaseInfos")]
    public class PurchaseInfo
    {
        [ForeignKey("Inventory")]
        public int  Id { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime When { get; set; }
        [Required]
        [StringLength(255)]
        public string Where { get; set; }
        [Required]
        [StringLength(255)]
        public string Warranty { get; set; }

        [Required]
        public int Price { get; set; }
       

        public virtual Inventory Inventory { get; set; }

    }
}