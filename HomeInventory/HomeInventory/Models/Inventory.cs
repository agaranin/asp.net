﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HomeInventory.Models
{
    public class Inventory
    {
        public int Id { get; set; }
        [Display(Name = "Photo")]
        public byte[] Photo { get; set; }
        [Required]
        [StringLength(255)]
        public string Description { get; set; }
        [Required]
        [StringLength(255)]
        public string LocationId { get; set; }
        public virtual Location Location { get; set; }
        [Required]
        [StringLength(255)]
        public string Model { get; set; }
        [Required]
        public int SerialNumber { get; set; }
        //public int PurchaseInfoId { get; set; }
        [Display(Name = "Purchase Info")]
        public virtual PurchaseInfo PurchaseInfo { get; set; }
    }
}