﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HomeInventory.Models
{
    public class PurchaseInfoViewModel
    {
        public int Id { get; set; }
        public DateTime When { get; set; }
        public string Where { get; set; }
        public string Warranty { get; set; }
        public int Price { get; set; }
    }
}