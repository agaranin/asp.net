﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeInventory.Models
{
    public class InventoryViewModel
    {
        public int Id { get; set; }
        public HttpPostedFileBase Photo { get; set; }
        public byte[] PhotoDB { get; set; }
        public string Description { get; set; }

        public string Location { get; set; }
        public string Model { get; set; }
        public int SerialNumber { get; set; }
    }
}