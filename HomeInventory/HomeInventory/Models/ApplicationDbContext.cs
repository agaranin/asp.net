﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace HomeInventory.Models
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<PurchaseInfo> PurchaseInfos { get; set; }
        public ApplicationDbContext() : base("DefaultConnection")
        {

        }
    }
}