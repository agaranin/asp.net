﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeInventory.Helpers;
using HomeInventory.Models;

namespace HomeInventory.Controllers
{
    public class InventoriesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Inventories
        public ActionResult Index()
        {
            var inventories = db.Inventories.Include(i => i.PurchaseInfo);
            return View(inventories.ToList());
        }

        // GET: Inventories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inventory inventory = db.Inventories.Find(id);
            if (inventory == null)
            {
                return HttpNotFound();
            }
            return View(inventory);
        }

        // GET: Inventories/Create
        public ActionResult Create()
        {
            ViewBag.Id = new SelectList(db.PurchaseInfos, "Id", "Where");
            return View();
        }

        // POST: Inventories/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection collection, HttpPostedFileBase Photo)
        {
            if (ModelState.IsValid)
            {
                var inventory = new Inventory();
                inventory.Description = collection["Description"];
                inventory.Location = collection["Location"];
                inventory.Model = collection["Model"];
                inventory.SerialNumber = int.Parse(collection["SerialNumber"]);
                if (Photo != null)
                    inventory.Photo = ImageConverter.ByteArrayFromPostedFile(Photo);
                db.Inventories.Add(inventory);
                PurchaseInfo purchaseInfo = new PurchaseInfo();
                purchaseInfo.When = DateTime.Parse(collection["When"]);
                purchaseInfo.Where = collection["Where"];
                purchaseInfo.Warranty = collection["Warranty"];
                purchaseInfo.Price = int.Parse(collection["Price"]);
                inventory.PurchaseInfo = purchaseInfo;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(collection);
        }

        // GET: Inventories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inventory inventory = db.Inventories.Find(id);
            if (inventory == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id = new SelectList(db.PurchaseInfos, "Id", "Where", inventory.Id);
            return View(inventory);
        }

        // POST: Inventories/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Photo,Description,Location,Model,SerialNumber")] Inventory inventoryVM, HttpPostedFileBase Photo)
        {
            if (ModelState.IsValid)
            {
                Inventory inventory = db.Inventories.Find(inventoryVM.Id);
                if (inventory != null && Photo != null)
                    inventory.Photo = ImageConverter.ByteArrayFromPostedFile(Photo);
                inventory.Description = inventoryVM.Description;
                inventory.Location = inventoryVM.Location;
                inventory.Model = inventoryVM.Model;
                inventory.SerialNumber = inventoryVM.SerialNumber;
                inventory.PurchaseInfo = inventoryVM.PurchaseInfo;
                db.Entry(inventory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(inventoryVM);
        }

        // GET: Inventories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Inventory inventory = db.Inventories.Find(id);
            if (inventory == null)
            {
                return HttpNotFound();
            }
            return View(inventory);
        }

        // POST: Inventories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Inventory inventory = db.Inventories.Find(id);
            PurchaseInfo purchaseInfo = db.PurchaseInfos.Find(id);
            db.PurchaseInfos.Remove(purchaseInfo);
            db.Inventories.Remove(inventory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
