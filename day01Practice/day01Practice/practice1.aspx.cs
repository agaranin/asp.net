﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace day01Practice
{
    public partial class practice1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {// Executes only on initial page 
                lblResult.Text = "Fill up the form";
            }// Rest of procedure executes on every request
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                var sex = chkMale.Checked ? "male" : "female";
                lblResult.Text = string.Format($"{txtStudentName.Text} is {sex}. The {ddlCourse.SelectedValue} course is {rblTechCoverage.SelectedValue} covered. Suggestions: {lbSuggestions.Text}. Updated ");
            }
        }
    }
}