﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="practice1.aspx.cs" Inherits="day01Practice.practice1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table style="width: 100%; padding: 10px; margin-top: 20px">
        <tr>
            <td>
                <asp:Label Text="Student name:" runat="server" /><br />
                <asp:TextBox runat="server" ID="txtStudentName" /><br />
                <asp:RequiredFieldValidator ForeColor="Red" ID="rfvStudentName" runat="server" ErrorMessage="Student name is required" ControlToValidate="txtStudentName"></asp:RequiredFieldValidator><br />
                <asp:Label Text="Sex" runat="server" />
                <asp:CheckBox ID="chkMale" runat="server" Text="Male" />
                <asp:CheckBox ID="chkFemale" runat="server" Text="Female" /><br /><br />
                <asp:Label Text="Select Course" runat="server" ID="lblCourse" />
                <asp:DropDownList ID="ddlCourse" runat="server">
                    <asp:ListItem>DOTNet</asp:ListItem>
                    <asp:ListItem>PHP</asp:ListItem>
                </asp:DropDownList><br /><br />
                <asp:Label Text="Technical Coverage" runat="server" />
                <asp:RadioButtonList ID="rblTechCoverage" runat="server">
                    <asp:ListItem>Excellent</asp:ListItem>
                    <asp:ListItem>Good</asp:ListItem>
                    <asp:ListItem>Average</asp:ListItem>
                    <asp:ListItem>Poor</asp:ListItem>
                </asp:RadioButtonList>
                <asp:Label Text="Suggestions:" runat="server" /><br />
                <asp:TextBox ID="lbSuggestions" runat="server" Width="300px" TextMode="MultiLine"></asp:TextBox>
                <asp:Button ID="btnSubmit" Text="Submit" runat="server" OnClick="btnSubmit_Click" /><br />
                <asp:Label ID="lblResult" runat="server" Font-Bold="true"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
