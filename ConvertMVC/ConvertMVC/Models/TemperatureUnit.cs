﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConvertMVC.Models
{
    public enum TemperatureUnit
    {
        Celsius,
        Fahrenheit,
        Kelvin
    }
}