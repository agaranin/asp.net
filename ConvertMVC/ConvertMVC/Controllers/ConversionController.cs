﻿using ConvertMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConvertMVC.Controllers
{
    public class ConversionController : Controller
    {
        // GET: Conversion
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Conversion conversion)
        {
            double temp = conversion.ToConvert;
            //string resultBag = "";

            switch (conversion.From)
            {
                case TemperatureUnit.Celsius:
                    switch (conversion.To)
                    {
                        case TemperatureUnit.Celsius:
                            conversion.Result = $"{temp:F2}°C";
                            break;
                        case TemperatureUnit.Fahrenheit:
                            temp = (temp * 1.8 + 32);
                            conversion.Result = $"{temp:F2}°F";
                            break;
                        case TemperatureUnit.Kelvin:
                            temp = temp + 273.15;
                            conversion.Result = $"{temp:F2}°K";
                            break;
                    }
                    break;
                case TemperatureUnit.Fahrenheit:
                    switch (conversion.To)
                    {
                        case TemperatureUnit.Celsius:
                            temp = (temp - 32) * 5 / 9;
                            conversion.Result = $"{temp:F2}°C";
                            break;
                        case TemperatureUnit.Fahrenheit:
                            conversion.Result = $"{temp:F2}°F";
                            break;
                        case TemperatureUnit.Kelvin:
                            temp = (temp - 32) * 5 / 9 + 273.15;
                            conversion.Result = $"{temp:F2}°K";
                            break;
                    }
                    break;
                case TemperatureUnit.Kelvin:
                    switch (conversion.To)
                    {
                        case TemperatureUnit.Celsius:
                            temp = temp - 273.15;
                            conversion.Result = $"{temp:F2}°C";
                            break;
                        case TemperatureUnit.Fahrenheit:
                            temp = (temp - 273.15) * 1.8 + 32;
                            conversion.Result = $"{temp:F2}°F";
                            break;
                        case TemperatureUnit.Kelvin:
                            conversion.Result = $"{temp:F2}°K";
                            break;
                    }
                    break;
            }
            return View(conversion);
            //return View("Result", result);
        }
    }
}