namespace FinalExam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateProps : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Runners", "EventId", "dbo.Events");
            DropPrimaryKey("dbo.Events");
            DropPrimaryKey("dbo.Runners");
            DropColumn("dbo.Events", "EventId");
            DropColumn("dbo.Runners", "RunnerId");
            AddColumn("dbo.Events", "Id", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.Runners", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Events", "Id");
            AddPrimaryKey("dbo.Runners", "Id");
            AddForeignKey("dbo.Runners", "EventId", "dbo.Events", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            AddColumn("dbo.Runners", "RunnerId", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.Events", "EventId", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.Runners", "EventId", "dbo.Events");
            DropPrimaryKey("dbo.Runners");
            DropPrimaryKey("dbo.Events");
            DropColumn("dbo.Runners", "Id");
            DropColumn("dbo.Events", "Id");
            AddPrimaryKey("dbo.Runners", "RunnerId");
            AddPrimaryKey("dbo.Events", "EventId");
            AddForeignKey("dbo.Runners", "EventId", "dbo.Events", "EventId", cascadeDelete: true);
        }
    }
}
