﻿namespace FinalExam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        EventId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 255),
                        EventDate = c.DateTime(nullable: false),
                        isClosed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.EventId);
            
            CreateTable(
                "dbo.Runners",
                c => new
                    {
                        RunnerId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 255),
                        LastName = c.String(nullable: false, maxLength: 255),
                        BirthDate = c.DateTime(nullable: false),
                        Gender = c.Int(nullable: false),
                        Email = c.String(nullable: false),
                        Telephone = c.String(nullable: false),
                        Address = c.String(),
                        PostalCode = c.String(),
                        Country = c.String(),
                        State = c.String(),
                        ContactName = c.String(),
                        ContactTelephone = c.String(),
                        EventId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RunnerId)
                .ForeignKey("dbo.Events", t => t.EventId, cascadeDelete: true)
                .Index(t => t.EventId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Runners", "EventId", "dbo.Events");
            DropIndex("dbo.Runners", new[] { "EventId" });
            DropTable("dbo.Runners");
            DropTable("dbo.Events");
        }
    }
}
