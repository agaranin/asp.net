namespace FinalExam.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addRegistrationDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Runners", "RegistrationDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Runners", "RegistrationDate");
        }
    }
}
