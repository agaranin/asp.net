﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FinalExam.Models
{
    public class ApplicationDbContext: DbContext
    {
        public DbSet<Runner> Runners { get; set; }
        public DbSet<Event> Events { get; set; }
        public ApplicationDbContext() : base("DefaultConnection")
        {

        }
    }
}