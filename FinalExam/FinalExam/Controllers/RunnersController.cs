﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalExam.Models;
using PagedList;

namespace FinalExam.Controllers
{
    public class RunnersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Runners
        public ActionResult Index(string sortDir, string currentFilter, string searchString, int? page , string sortOrder = "")
        {
            if (searchString != null)
                page = 1;
            else
                searchString = currentFilter;

            ViewBag.CurrentFilter = searchString;
            ViewBag.sortOrder = sortOrder;
            ViewBag.sortDir = sortDir;

            var query = from r in db.Runners.Include(r => r.Event).AsQueryable()
                        select new RunnerViewModelForList() { 
                            RunnerId = r.Id,
                            Name = r.FirstName + " " + r.LastName,
                            Email = r.Email,
                            Gender = r.Gender.ToString(),
                            Telephone = r.Telephone,
                            EventName = r.Event.Name,
                            EventStatus = r.Event.IsClosed ? "Closed" : "Open"
                        };
            if (!string.IsNullOrEmpty(searchString))
                query = query.Where(s => s.Name.Contains(searchString));
            switch (sortOrder.ToLower())
            {
                case "name":
                        if (sortDir.ToLower() == "desc")
                            query = query.OrderByDescending(p => p.Name);
                        else
                            query = query.OrderBy(p => p.Name);
                    break;
                case "gender":
                    if (sortDir.ToLower() == "desc")
                        query = query.OrderByDescending(p => p.Gender);
                    else
                        query = query.OrderBy(p => p.Gender);
                    break;
                case "email":
                    if (sortDir.ToLower() == "desc")
                        query = query.OrderByDescending(p => p.Email);
                    else
                        query = query.OrderBy(p => p.Email);
                    break;
                case "telephone":
                    if (sortDir.ToLower() == "desc")
                        query = query.OrderByDescending(p => p.Telephone);
                    else
                        query = query.OrderBy(p => p.Telephone);
                    break;
                case "eventname":
                    if (sortDir.ToLower() == "desc")
                        query = query.OrderByDescending(p => p.EventName);
                    else
                        query = query.OrderBy(p => p.EventName);
                    break;
                case "eventstatus":
                    if (sortDir.ToLower() == "desc")
                        query = query.OrderByDescending(p => p.EventStatus);
                    else
                        query = query.OrderBy(p => p.EventStatus);
                    break;
                default:
                    query = query.OrderBy(p => p.Name);
                    break;
            }

            int pageSize = 2;
            int pageNumber = (page ?? 1);
            var data = query.ToPagedList(pageNumber, pageSize);
            if (Request.IsAjaxRequest())
                return PartialView("_RunnersList", data);
            else
            return View(data);
        }

        // GET: Runners/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Runner runner = db.Runners.Find(id);
            if (runner == null)
            {
                return HttpNotFound();
            }
            return View(runner);
        }

        // GET: Runners/Create
        public ActionResult Create()
        {
            ViewBag.EventId = new SelectList(db.Events, "Id", "Name");
            return View();
        }

        // POST: Runners/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RunnerViewModelEdit runnerVM)
        {
            if (ModelState.IsValid)
            {
                Runner runner = new Runner()
                {
                    FirstName = runnerVM.FirstName,
                    LastName = runnerVM.LastName,
                    Email = runnerVM.Email,
                    Gender = runnerVM.Gender,
                    BirthDate = runnerVM.BirthDate,
                    Address = runnerVM.Address,
                    Country = runnerVM.Country,
                    EventId = runnerVM.EventId,
                    Telephone = runnerVM.Telephone,
                    State = runnerVM.State,
                    PostalCode = runnerVM.PostalCode,
                    RegistrationDate = DateTime.Now,
                    ContactName = runnerVM.ContactName,
                    ContactTelephone = runnerVM.ContactTelephone
                };

                if (!IsEventClosed(runnerVM.EventId))
                {
                    db.Runners.Add(runner);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                    ModelState.AddModelError("EventId", "The selected event is closed.");
            }

            ViewBag.EventId = new SelectList(db.Events, "Id", "Name", runnerVM.EventId);
            return View(runnerVM);
        }

        private RunnerViewModelEdit GetRunnerViewModel(Runner runner)
        {
            RunnerViewModelEdit runnerVM = new RunnerViewModelEdit()
            {
                Id = runner.Id,
                FirstName = runner.FirstName,
                LastName = runner.LastName,
                Email = runner.Email,
                Gender = runner.Gender,
                BirthDate = runner.BirthDate,
                Address = runner.Address,
                Country = runner.Country,
                EventId = runner.EventId,
                Telephone = runner.Telephone,
                State = runner.State,
                PostalCode = runner.PostalCode,
                RegistrationDate = DateTime.Now,
                ContactName = runner.ContactName,
                ContactTelephone = runner.ContactTelephone
            };
            return runnerVM;
        }

        // GET: Runners/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Runner runner = db.Runners.Find(id);
            if (runner == null)
            {
                return HttpNotFound();
            }
            var runnerVM = GetRunnerViewModel(runner);

            ViewBag.EventId = new SelectList(db.Events, "Id", "Name", runnerVM.EventId);
            return View(runnerVM);
        }

        private bool IsEventClosed(int eventId)
        {
            Event @event = db.Events.Where(e => e.Id == eventId).FirstOrDefault();
            if (@event != null && @event.IsClosed)
                return true;
            return false;
        }

        // POST: Runners/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(RunnerViewModelEdit runnerVM)
        {
            if (ModelState.IsValid)
            {
                Runner runner = db.Runners.Find(runnerVM.Id);
                runner.FirstName = runnerVM.FirstName;
                runner.LastName = runnerVM.LastName;
                runner.Email = runnerVM.Email;
                runner.Gender = runnerVM.Gender;
                runner.BirthDate = runnerVM.BirthDate;
                runner.Address = runnerVM.Address;
                runner.Country = runnerVM.Country;
                runner.EventId = runnerVM.EventId;
                runner.Telephone = runnerVM.Telephone;
                runner.State = runnerVM.State;
                runner.PostalCode = runnerVM.PostalCode;
                runner.ContactName = runnerVM.ContactName;
                runner.ContactTelephone = runnerVM.ContactTelephone;

                if (!IsEventClosed(runnerVM.EventId))
                {
                    db.Entry(runner).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                    ModelState.AddModelError("EventId", "The selected event is closed.");
            }

            ViewBag.EventId = new SelectList(db.Events, "Id", "Name", runnerVM.EventId);
            return View(runnerVM);
        }

        // GET: Runners/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Runner runner = db.Runners.Find(id);
            if (runner == null)
            {
                return HttpNotFound();
            }
            return View(runner);
        }

        // POST: Runners/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Runner runner = db.Runners.Find(id);
            db.Runners.Remove(runner);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
