﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalExam
{
    [HubName("myHub")]
    public class MyHub : Hub
    {
        public static void UpdateFromServer()
        {
            var hubContext = GlobalHost.ConnectionManager.GetHubContext<MyHub>();
            hubContext.Clients.All.updateClient();
        }
    }
}